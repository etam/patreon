#!/usr/bin/python3

import argparse
import json
import os.path
import shutil
import sys
import urllib.parse
import urllib.request


def urlopen(url):
    request = urllib.request.Request(
        url,
        headers={'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0'}
    )
    return urllib.request.urlopen(request)


def make_stream_url(user_id):
    return 'https://api.patreon.com/stream?' + urllib.parse.urlencode({
        'include': ','.join([
            'user.null',
            'attachments.null',
            'user_defined_tags.null',
            'campaign.earnings_visibility',
        ]),
        'fields[post]': ','.join([
            'post_type',
            'post_file',
        ]),
        'fields[user]': '',
        'fields[campaign]': '',
        'page[cursor]': 'null',
        'filter[is_by_creator]': 'true',
        'filter[is_following]': 'false',
        'filter[creator_id]': user_id,
        'filter[contains_exclusive_posts]': 'true',
        'json-api-version': '1.0',
    })


def patreon_images_from_data(data):
    """
    generates dicts
    {
        "name": "file.png",
        "url": "https://example.com/file.png",
    }
    """
    for post in data:
        if not post['type'] == 'post':
            continue
        attrs = post['attributes']
        if not attrs['post_type'] == 'image_file':
            continue
        post_file = attrs.get('post_file')
        if not post_file:
            continue
        yield post_file


def patreon_images_of_user(user_id):
    stream_url = make_stream_url(user_id)
    while stream_url:
        with urlopen(stream_url) as f:
            stream = json.loads(f.read().decode('utf-8'))
        for img in patreon_images_from_data(stream['data']):
            yield img
        stream_url = stream['links'].get('next')
        if stream_url:
            stream_url = 'https://' + stream_url


def save_as(url, filename):
    with urlopen(url) as response, \
         open(filename, 'wb') as out_file:
        shutil.copyfileobj(response, out_file)


def parse_args(argv):
    parser = argparse.ArgumentParser(
        description='download public images from patreon')
    parser.add_argument('user_id',
                        type=int)
    parser.add_argument('--limit',
                        type=int,
                        help='maximum number of images to check')
    return parser.parse_args(argv)


def main(argv):
    args = parse_args(argv)

    i = 0
    for img in patreon_images_of_user(args.user_id):
        if args.limit:
            if i < args.limit:
                i += 1
            else:
                break

        if os.path.exists(img['name']):
            print("'{}' as '{}' already downloaded"
                  .format(img['url'], img['name']))
            continue

        print("downloading '{}' as '{}'".format(img['url'], img['name']))
        save_as(img['url'], img['name'])


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
