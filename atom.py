#!/usr/bin/python3

import cgi
import html
import json
import urllib.parse
import urllib.request

import cgitb
cgitb.enable(display=0)
def urlopen(url):
    request = urllib.request.Request(
        url,
        headers={'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0'}
    )
    return urllib.request.urlopen(request)


def make_stream_url(user_id):
    return 'https://api.patreon.com/stream?' + urllib.parse.urlencode({
        'include': ','.join([
            'user.null',
            'attachments.null',
            'user_defined_tags.null',
            'campaign.earnings_visibility',
        ]),
        'fields[post]': ','.join([
            'content',
            'post_file',
            'post_type',
            'published_at',
            'title',
            'url',
        ]),
        'fields[user]': ','.join([
            'full_name',
            'url',
        ]),
        'fields[campaign]': '',
        'page[cursor]': 'null',
        'filter[is_by_creator]': 'true',
        'filter[is_following]': 'false',
        'filter[creator_id]': user_id,
        'filter[contains_exclusive_posts]': 'true',
        'json-api-version': '1.0',
    })


def get_stream(user_id):
    with urlopen(make_stream_url(user_id)) as f:
        return json.loads(f.read().decode('utf-8'))


def get_user_info(stream):
    for obj in stream['included']:
        if obj['type'] == 'user':
            return obj['attributes']


def print_header(stream):
    user_info = get_user_info(stream)
    print("""\
Content-Type: application/atom+xml

<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
<title>Patreon: {full_name}</title>
<link href="{url}"/>
<updated>{updated}</updated>
<author><name>{full_name}</name></author>
<id>{url}</id>"""
          .format(full_name=user_info['full_name'],
                  url=user_info['url'],
                  updated=stream['data'][0]['attributes']['published_at']))


def html_escape(txt):
    return html.escape(txt)\
               .encode('ascii', 'xmlcharrefreplace')\
               .decode('ascii')


def print_entry(attrs):
    # this encode/decode seems bad, but I have no idea how to do it better
    content = attrs.get('content', '')
    if attrs['post_type'] == 'image_file' and 'post_file' in attrs:
        post_file = attrs['post_file']
        content = "<img src=\"{url}\" alt=\"{name}\" /><br>"\
                  .format(url=post_file['url'],
                          name=post_file['name']) + \
                  content

    title = html_escape(attrs['title'])
    content = html_escape(content)

    print("""\
<entry>
    <title>{title}</title>
    <link href="{url}"/>
    <id>{url}</id>
    <updated>{updated}</updated>
    <content type="html">{content}</content>
</entry>""".format(title=title,
                   url=attrs['url'],
                   updated=attrs['published_at'],
                   content=content))


def main():
    user_id = int(cgi.FieldStorage().getfirst('user_id'))
    stream = get_stream(user_id)

    print_header(stream)

    for post in stream['data']:
        print_entry(post['attributes'])

    print("</feed>")


main()
